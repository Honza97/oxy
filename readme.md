**Příprava prostředí na OS Windows:**

Stáhneme a nainstalujeme si program XAMPP, který nám zajistí celé prostředí (Apache, PHP, MySQL, phpMyAdmin atd.). Program můžeme stáhnout odsud: `https://www.apachefriends.org/index.html`, stahujeme verzi programu s PHP 7 a vyšším. Poté ho již stačí pouze nainstalovat.
Nesmíme zapomenout ani na program Composer, taktéž ho stáhneme a nainstalujeme (https://getcomposer.org).  Prostředí je tímto připraveno. 

**Inicializace projektu:**
1) Do složky `htdocs` (uvnitř program XAMPP) naklonujeme projekt z BitBucketu (`git clone https://Honza97@bitbucket.org/Honza97/oxy.git`)
2) Vytvoříme databázi s kódováním `utf8_general_ci` pomocí aplikace phpMyAdmin nebo jiné
3) Přepneme se do složky `rest`, která se nachází uvnitř projektu
4) Zde přejmenujeme soubor `.env.example` na pouze `.env` a editujeme (nastavíme připojení k databázi)
5) Následně spustíme příkazy:
    - `composer install`
    - `php artisan key:generate`
    - `php artisan migrate`
6) Inicializace dokončena, projekt by měl být dostupný na: `http://localhost/oxy/front`


**Popis řešení:**

Systém jsem rozdělil do 2 aplikací. Složka `rest` reprezentuje REST API, které je postaveno na frameworku Laravel. Aplikace obsahuje i strukturu databáze, která se vytváří a aktualizuje pomocí migrací.

Druhou část reprezentuje složka `front`, kterou považuji jako pouze nějaký výňatek z další aplikace. Proto na tuto část nebyl použit žádný framework ani návrhový model. Jedná se o jednoduché grafické rozhraní s cURL požadavky na zmíněné REST API.
V případě, že bych měl více času, postavil bych tuto část také na Laravelu (případně jiném frameworku), kde bych použil šablonovací systém "blade" nebo "twig" a oddělil tím i logiku od grafiky.