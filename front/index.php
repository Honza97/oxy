<?php
function getUsers()
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL            => "http://localhost/oxy/rest/public/getUsers",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => "",
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 30,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST  => "GET",
        CURLOPT_POSTFIELDS     => "{}",
        CURLOPT_HTTPHEADER     => array(
            "Content-Type: application/json",
            "cache-control: no-cache"
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    return json_decode($response)->users ?? [];
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Uživatelé</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/simple-sidebar.css" rel="stylesheet">
</head>

<body>
<div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading" style="font-weight: bold">Uživatelé</div>
        <div class="list-group list-group-flush">
            <a href="index.php" class="list-group-item list-group-item-action bg-light">Seznam uživatelů</a>
            <a href="register.php" class="list-group-item list-group-item-action bg-light">Registrovat uživatele</a>
        </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <h1 class="mt-4">Seznam uživatelů</h1>
            <table class="table">
                <thead>
                <th>Jméno</th>
                <th>Heslo</th>
                <th>E-mail</th>
                <th>Uživatelská role</th>
                <th>Vytvořen</th>
                </thead>
                <tbody>
                <?php
                foreach (getUsers() as $user) {
                    ?>
                    <tr>
                        <td><?php echo $user->name ?></td>
                        <td><?php echo $user->password ?></td>
                        <td><?php echo $user->email ?></td>
                        <td><?php echo $user->role ?></td>
                        <td><?php echo $user->created_at ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<!-- /#wrapper -->
<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
