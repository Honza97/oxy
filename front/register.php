<?php
function register($name, $password, $email, $admin)
{
    $password = sha1($password);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL            => "http://localhost/oxy/rest/public/register",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => "",
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 30,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST  => "POST",
        CURLOPT_POSTFIELDS     => "{\n\t\"name\":\"$name\",\n\t\"password\": \"$password\",\n\t\"email\": \"$email\",\n\t\"admin\": $admin\n}",
        CURLOPT_HTTPHEADER     => array(
            "Content-Type: application/json",
            "cache-control: no-cache"
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    return json_decode($response);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Uživatelé</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/simple-sidebar.css" rel="stylesheet">
</head>

<body>
<div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading" style="font-weight: bold">Uživatelé</div>
        <div class="list-group list-group-flush">
            <a href="index.php" class="list-group-item list-group-item-action bg-light">Seznam uživatelů</a>
            <a href="register.php" class="list-group-item list-group-item-action bg-light">Registrovat uživatele</a>
        </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <h1 class="mt-4" style="margin-bottom: 40px">Registrovat uživatele</h1>

            <?php if (!isset($_POST['submit'])) { ?>
                <div class="row">
                    <div class="col col-lg-6">
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="name">Jméno:</label>
                                <input type="text" name="name" id="name" class="form-control" required
                                       placeholder="Jméno">
                            </div>
                            <div class="form-group">
                                <label for="paswword">Heslo:</label>
                                <input type="password" name="password" id="password" class="form-control" required
                                       placeholder="Heslo">
                            </div>
                            <div class="form-group">
                                <label for="email">Heslo:</label>
                                <input type="email" name="email" id="email" class="form-control" required
                                       placeholder="E-mail">
                            </div>
                            <div class="form-group">
                                <label>Role: </label>
                                <div class="radio">
                                    <label><input type="radio" name="admin" checked value="0"> Uživatel</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="admin" value="1"> Administátor</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Registrovat" class="btn btn-danger" name="submit">
                            </div>
                        </form>
                    </div>
                </div>
            <?php } else {
                $response = register($_POST['name'], $_POST['password'], $_POST['email'], $_POST['admin']);

                if ($response->status) {
                    echo '<p style="color: green">Uživatel byl úspěšně registrován s id: ' . $response->id . '</p>';
                } else {
                    echo '<p style="color: red;">' . $response->info . '</p>';
                }
            } ?>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<!-- /#wrapper -->
<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
