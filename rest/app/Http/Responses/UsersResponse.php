<?php

namespace App\Http\Responses;

use App\User;
use Illuminate\Support\Collection;

class UsersResponse
{
    /**
     * @param Collection $users
     * @return array
     */
    public function transform(Collection $users): array
    {
        return [
            'users' => $users->reduce(function (array $carry, User $user) {
                $carry[] = [
                    'name'       => $user->name,
                    'password'   => '***',
                    'email'      => $user->email,
                    'role'       => $user->admin == 1 ? 'Administrátor' : 'Uživatel',
                    'created_at' => $user->created_at->toDateTimeString(),
                ];

                return $carry;
            }, []),
        ];
    }
}