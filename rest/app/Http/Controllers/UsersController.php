<?php

namespace App\Http\Controllers;

use App\Http\Responses\UsersResponse;
use App\Repositories\UsersRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{

    /** @var UsersRepository */
    private $userRepository;

    public function __construct(UsersRepository $usersRepository)
    {
        $this->userRepository = $usersRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required|unique:users',
            'password' => 'required',
            'email'    => 'required|email|unique:users'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'info'   => $validator->messages()->first()
            ]);
        }

        $user = $this->userRepository->create($request->all());
        return response()->json([
            'status' => true,
            'id'     => $user->id
        ]);
    }

    /**
     * @return array
     */
    public function getUsers(): array
    {
        $users = $this->userRepository->all();
        return (new UsersResponse())->transform($users);
    }
}
