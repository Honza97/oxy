<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Collection;

class UsersRepository
{

    /**
     * @param array $data
     * @return User
     */
    public function create(array $data): User
    {
        return User::query()->create($data);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return User::all();
    }

}